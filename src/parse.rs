#[derive(Debug)]
pub enum IRCMessage {
    Nick(String),
    User(String, String),
    Ping(String),
    Pong(String),
    Join(String, String),
    Part(String, String, String),
    Notice(String, String, String),
    PrivMsg(String, String, String),
    Unknown(String),
    Quit(String, String),
    Nothing
}

impl IRCMessage {
    /// Turns the object into a sendable string.
    pub fn to_string(self) -> Result<String, String> {
        match self {
            IRCMessage::Nick(name) => {
                Ok(format!("NICK {}\r\n", name))
            },
            IRCMessage::User(name, realname) => {
                Ok(format!("USER {} 0 * :{}\r\n", name, realname))
            },
            IRCMessage::Ping(data) => {
                Ok(format!("PING {}\r\n", data))
            },
            IRCMessage::Pong(data) => {
                Ok(format!("PONG {}\r\n", data))
            },
            IRCMessage::Join(_, channel) => {
                //println!("joining \r");
                Ok(format!("JOIN {}\r\n", channel))
            },
            IRCMessage::Part(_, channel, _) => {
                Ok(format!("PART {}\r\n", channel))
            }
            IRCMessage::Notice(_, target, message) => {
                Ok(format!("NOTICE {} {}\r\n", target, message))
            },
            IRCMessage::PrivMsg(_, target, message) => {
                Ok(format!("PRIVMSG {} {}\r\n", target, message))
            },
            IRCMessage::Quit(_, _) => {
                Ok("QUIT\r\n".to_string())
            },
            IRCMessage::Unknown(data) => {
                Err(data)
            },
            IRCMessage::Nothing => {
                Err("".to_string())
            }
        }
    } 

    /// Parses a string from the server into an object. 
    pub fn from_string(s: &str) -> Self {
        let mut words = s.split_whitespace();

        let prefix = if s.starts_with(':') {
            words.next()
        } else {
            None
        };

        let source = prefix.unwrap_or("").split(':').nth(1).unwrap_or("").split("!").next().unwrap_or("").to_string();

        let cmd = words.next();
        if cmd.is_none() {
            return IRCMessage::Nothing;
        }

        // Because mozilla puts ":hostname" in front, while quakenet does not.
        let mut cmd = cmd.unwrap();
        /*if cmd.starts_with(":") {
            let newcmd = words.next();
            if newcmd.is_some() {
                cmd = newcmd.unwrap();
            }
        }*/

        //println!("cmd = {}", cmd);
        match cmd {
            "NOTICE" => {
                let sender = words.next().unwrap().to_string();
                let rest: Vec<&str> = words.collect();
                let rest = rest.join(" ");
                IRCMessage::Notice(source, sender, rest)
            },
            "PRIVMSG" => {
                let sender = words.next().unwrap().to_string();
                let rest: Vec<&str> = words.collect();
                let rest = rest.join(" ").to_string();
                IRCMessage::PrivMsg(source, sender, rest)
            },
            "PING" => {
                let data: Vec<&str> = words.collect();
                let data = data.join(" ").to_string();
                IRCMessage::Ping(data)
            },
            "JOIN" => {
                let chan = words.next().unwrap().to_string();
                IRCMessage::Join(source, chan)
            },
            "PART" => {
                let chan = words.next().unwrap().to_string();
                let message: Vec<&str> = words.collect();
                let message = message.join(" ");
                IRCMessage::Part(source, chan, message)
            }
            _ => {
                let data: Vec<&str> = words.collect();
                let data = data.join(" ").to_string();
                IRCMessage::Unknown(format!("{} {} {}", source, cmd, data))
            }
        }
    }
}
