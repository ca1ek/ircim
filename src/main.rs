extern crate termion;
use termion::{color, style};
use termion::raw::IntoRawMode;
use termion::event::Key;
use termion::input::TermRead;

mod parse;
use parse::IRCMessage;

use std::env;
use std::io::{stdin, stdout, Read, Write, Result};
use std::net::{TcpStream, ToSocketAddrs};
use std::str;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::{channel, Sender, Receiver};
use std::sync::atomic::{AtomicBool, Ordering};
use std::thread;

use std::cell::UnsafeCell;

/// Redox domain socket
pub struct Socket {
    file: UnsafeCell<TcpStream>
}

unsafe impl Send for Socket {}
unsafe impl Sync for Socket {}

impl Socket {
    pub fn connect<A: ToSocketAddrs>(addr: A) -> Result<Socket> {
        let file = try!(TcpStream::connect(addr));
        Ok(Socket {
            file: UnsafeCell::new(file)
        })
    }

    pub fn receive(&self, buf: &mut [u8]) -> Result<usize> {
        unsafe { (*self.file.get()).read(buf) }
    }

    pub fn send(&self, buf: &[u8]) -> Result<usize> {
        unsafe { (*self.file.get()).write(buf) }
    }
}


fn main() {
    use std::num::Wrapping;

    let mut args = env::args().skip(1);

    let nick = args.next().expect("No nickname provided");

    // TCP connections
    let socket_write = Arc::new(Socket::connect("irc.mozilla.org:6667").expect("Failed to connect to irc.mozilla.org"));
    let socket_read = socket_write.clone();

    let terminate_main = Arc::new(AtomicBool::new(false));
    let terminate_receive = terminate_main.clone();
    let terminate_transmit = terminate_main.clone();
    let terminate_stdin = terminate_main.clone();

    // Thread channels
    //
    // `transmit_thread` reads `transmit_read` for messages from main() and sends them out
    // `receive_thread` listens for messages on TCP and writes them to `receive_write`
    // `stdin_thread` keeps reading keys from stdin and pushing them onto `keys_write`
    // Main thread controls all of the user interface.
    let (transmit_write, transmit_read): (Sender<IRCMessage>, Receiver<IRCMessage>) = channel();
    let (receive_write, receive_read): (Sender<IRCMessage>, Receiver<IRCMessage>) = channel();
    let (keys_write, keys_read): (Sender<Key>, Receiver<Key>) = channel();

    let receive_thread = thread::spawn(move|| {
        let socket_read = socket_read; // Move TCP reading socket to this thread
        let receive_write = receive_write; // Move thread -> main() sender here
        let terminate = terminate_receive;
        'read: loop {
            let shutdown = terminate.load(Ordering::Relaxed);

            if shutdown { break 'read; }

            let mut buffer: [u8; 65535] = [0; 65535];
            let bytes_read = socket_read.receive(&mut buffer).expect("Something broke while reading the socket in 'read loop");
            if bytes_read == 0 {
                break 'read;
            }

            let slice = &buffer[0 .. bytes_read];

            let s = str::from_utf8(&slice).expect("UTF-8 fail. Kinda unexpected but not really.");
            let broken_up = s.split("\n");
            for message in broken_up {
                //print!("receive_thread: {}", message);
                receive_write.send(IRCMessage::from_string(message));
            }
        }
    });

    let transmit_thread = thread::spawn(move|| {
        let socket_write = socket_write; // Move TCP writing socket to this thread
        let transmit_read = transmit_read; // Move main() -> thread receiver here.
        let terminate = terminate_transmit;
        'write: loop {
            let shutdown = terminate.load(Ordering::Relaxed);

            if shutdown { break 'write; }

            let message = transmit_read.recv().expect("Reading from transmit_read failed.");
            let string_msg = message.to_string();
            //println!("{}", string_msg);
            if string_msg.is_ok() {
               socket_write.send(string_msg.unwrap().as_bytes());
            }
        }
    });

    let stdin_thread = thread::spawn(move|| {
        let keys_write = keys_write;
        let terminate = terminate_stdin;
        'stdin: loop {
            let shutdown = terminate.load(Ordering::Relaxed);

            if shutdown { break 'stdin; }

            for key in stdin().keys() {
                if key.is_ok() {
                    keys_write.send(key.unwrap());
                }
            }
        }
    });

    // Authenticate with the network.
    transmit_write.send(IRCMessage::Nick(nick.to_string()));
    transmit_write.send(IRCMessage::User(nick.to_string(), nick.to_string()));

    //transmit_write.send(IRCMessage::Join("#testing-lol".to_string()));

    let mut stdout = stdout().into_raw_mode().unwrap();
    //let stdin = stdin();

    'control_loop: loop {
        let key = keys_read.try_recv(); 

        let mut quit = false;
        if key.is_ok() {
            let key = key.unwrap();

            match key {
                Key::Char('j') => {
                    //write!(stdout, "J!!!!!!\r\n");
                    transmit_write.send(IRCMessage::Join("".to_string(), "#testing-lol".to_string()));
                },
                Key::Char('q') => {
                    transmit_write.send(IRCMessage::Quit("".to_string(), "".to_string()));
                    quit = true;
                    //terminate_main.store(false, Ordering::Relaxed);
                    //break 'control_loop;
                },
                _ => {}
            }
        }

        let message = receive_read.try_recv();
        if message.is_ok() {
            let message = message.unwrap();
            match message {
                IRCMessage::Nothing => {},
                msg => {write!(stdout, "msg: {:?}\r\n", msg);},
            }
        }

        if quit {break 'control_loop;}

        stdout.flush().unwrap();

    }

    // LEAVE NO CHILD BEHIND!.
    // Actually, now, leave them behind.
    //write!(stdout, "Joining transmit_thread\r\n");
    //transmit_thread.join();
    //write!(stdout, "Joining receive_thread\r\n");
    //receive_thread.join();
}
